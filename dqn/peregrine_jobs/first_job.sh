#!/bin/bash
#SBATCH --time=05:30:00
#SBATCH --nodes=1
#SBATCH --ntasks=10
#SBATCH --job-name=first_job
#SBATCH --mem=8000

module load TensorFlow/1.12.0-foss-2018a-Python-3.6.4

python /home/s3806855/rug-ml/dqn/training_job.py /home/s3806855/rug-ml/dqn/settings/settings_1.json /home/s3806855/rug-ml/dqn/settings/hps_cartpole_v0_1.json /home/s3806855/rug-ml/dqn/results/

