#!/bin/bash
#SBATCH --time=24:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --job-name=si_s2_h5_gpu
#SBATCH --partition=gpu
#SBATCH --gres=gpu:1
#SBATCH --mem=64000

module load TensorFlow/1.10.1-fosscuda-2018a-Python-3.6.4

python /home/s3806855/rug-ml/dqn/training_job.py /home/s3806855/rug-ml/dqn/settings/codrut/settings_2.json /home/s3806855/rug-ml/dqn/settings/codrut/hps_space_invaders_5.json /home/s3806855/rug-ml/dqn/results/

