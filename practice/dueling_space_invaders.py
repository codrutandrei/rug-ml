import numpy as np
import gym
import random
from collections import deque
import cv2
np.random.seed(1337)
random.seed(1337)

from keras.models import Sequential, load_model, Model
from keras.layers import Dense, Conv2D, Flatten, Dropout, Reshape, Input, Lambda, Concatenate, merge, Add
from keras import backend as K
from keras.optimizers import Adam


class Qspace():

    def __init__(self, load_weights=False):
        self.load_weights=load_weights
        # initialize hyperparameters
        self.epsilon = 1.0
        self.epsilon_decay_rate = 0.995
        self.epsilon_min = 0.01
        self.gamma = 0.999
        self.alpha = 0.00025
        self.batch_size = 64
        self.memories = deque(maxlen=1000000)
        self.n_episodes = 100000
        self.n_steps = 10000
        self.preprocessed_frames_buffer = deque(maxlen=3)
        self.reward_list = [0]
        # initialize environment
        self.env = gym.make('SpaceInvaders-v0')
        self.state_size = self.env.observation_space
        self.action_size = self.env.action_space.n
        self.process_buffer = []
        self.num_frames = 3
        # initialize networks
        self.q_network = self.build_network()
        self.target_network = self.build_network()
        self.update_target_network()

    def build_network(self):
        input_layer = Input(shape=(84, 84, self.num_frames))
        conv1 = Conv2D(filters=32, kernel_size=(8, 8), strides=(4, 4),
                         activation='relu')(input_layer)
        conv2 = Conv2D(64, kernel_size=(4, 4), strides=(2, 2), activation='relu')(conv1)
        conv3 = (Conv2D(64, kernel_size=(3, 3), activation='relu'))(conv2)
        flatten = Flatten()(conv3)
        dense1 = Dense(512, activation='relu')(flatten)
        dense2 = Dense(512, activation='relu')(flatten)
        state_value = Dense(1)(dense1)
        action_advantage = Dense(self.action_size)(dense2)
        sum_action_advantage = Lambda(lambda x: x - K.mean(x))(action_advantage)
        model = Add()([sum_action_advantage, state_value])
        network = Model(inputs=input_layer, outputs=model)
        network.compile(loss='mse', optimizer=Adam(lr=self.alpha))
        return network

    def save_networks(self):
        self.q_network.save_weights('dueling_space_invaders_qnetwork_weights.h5')
        self.target_network.save_weights('dueling_space_invaders_target_weights.h5')


    def load_networks(self):
        self.target_network.load_weights('dueling_space_invaders_target_weights.h5')
        self.q_network.load_weights('dueling_space_invaders_qnetwork_weights.h5')


    def update_target_network(self):
        self.target_network.set_weights(self.q_network.get_weights())


    def init_frame(self):
        # create 3 initial frames
        self.env.reset()
        s1, r1, _, _ = self.env.step(0)
        s2, r2, _, _ = self.env.step(0)
        s3, r3, _, _ = self.env.step(0)
        self.process_buffer = [s1, s2, s3]


    def preprocess(self):
        # convert each frame to gray
        # resize the frame to 90x84
        # crop a part of the frame is does not provide any useful information
        # return stack of three frames
        for i, frame in enumerate(self.process_buffer):
            frame_gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
            resized_frame = cv2.resize(frame_gray, (84, 90))
            resized_frame = resized_frame[1:85, :, np.newaxis]
            self.preprocessed_frames_buffer.append(resized_frame)
        return np.concatenate(self.preprocessed_frames_buffer, axis=2)


    def select_action(self, current_state):
        if random.random() <= self.epsilon:
            return self.env.action_space.sample()
        else:
            qvalue = self.q_network.predict(current_state)
            return np.argmax(qvalue[0])



    def add_memory(self, current_state, action, reward, next_state, done):
        self.memories.append((current_state, action, reward, next_state, done))


    def replay_experience(self):

        mini_experience = random.sample(self.memories,
                                        min(len(self.memories), self.batch_size))

        for current_state, action, reward, next_state, done in mini_experience:
            target = reward
            if not done:
                #calculate target with the target network which is update every episode
                target = (reward + self.gamma * np.max(self.target_network.predict(next_state)[0]))
            target_q = self.q_network.predict(current_state)
            target_q[0][action] = target
            self.q_network.fit(current_state, target_q, epochs=1, verbose=0)


    def epsilon_decay(self):
        self.epsilon = max(self.epsilon * self.epsilon_decay_rate, self.epsilon_min)


    def reshape_state(self, state):
        return np.reshape(state, [1, 84, 84, self.num_frames])


    def run(self):
        if self.load_weights:
            self.load_networks()

        steps = 1
        #self.init_frame()
        for i in range(self.n_episodes):
            self.init_frame()
            total_reward = 0
            done = False
            current_state = self.preprocess()
            current_state = self.reshape_state(current_state)

            # have to reshape the state
            # self.env.render()
            while steps <= self.n_steps and done!=True:  # probably this condition is wrong
                self.env.render()
                # get the next 3 frames and append them to buffer
                stack_reward = 0
                for i in range(self.num_frames):

                    action = self.select_action(current_state)

                    temp_next_state, reward, temp_done, _ = self.env.step(action)

                    stack_reward += reward

                    # add the new state in the buffer for preprocessing
                    self.process_buffer.append(temp_next_state)
                    # if one out of 3 frames is done, then we consider them all done
                    if temp_done:
                        total_reward = 0
                        done = True
                next_state = self.preprocess()
                next_state = self.reshape_state(next_state)

                self.add_memory(current_state, action, stack_reward, next_state, done)
                total_reward += stack_reward
                print("the total reward is: " , total_reward)
                if total_reward > max(self.reward_list):
                    self.reward_list.append(total_reward)
                current_state = next_state

                self.replay_experience()
                self.epsilon_decay()


                steps += 1
            print("the list of the rewards :", self.reward_list)
            #updat target network after 10000 steps
            if steps%10000==0:
                self.update_target_network
                self.save_networks()
                steps = 1


space = Qspace(load_weights=False)
space.run()

