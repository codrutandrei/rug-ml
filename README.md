# rug-ml

Repository for Machine Learning project (RUG, 2018-2019)
Team members:  
1. Codrut Andrei Diaconu (Computing Science, Data Science and Systems Complexity)  
2. George Doulgeris (Artificial Intelligence)  
3. Orest Divintari (Computing Science)  
4. Marios Lykiardopoulos (Computing Science)  

The project is focused on learning to play (simple) games using Reinforcement Learning techniques. For environments we use OpenAI Gym library.

For running the program you need Python3 with the requirements installed from the **requirements.txt** file. Installing the requirements can be done by running in terminal:  
   ----- $ pip install -r **requirements.txt**

## For running the code use the following command:
   ----- $ python3 **training_job.py**     **settings.json**  **hyperparameters.json**    **directory_for_saving_the_checkpoints**

where:   
	* **training_job.py** is a Python script which build the agents and start the training (the implementation of the algorithms can be found in dqn_agent.py; it doesn't need any modification, all the paramters are set by using the JSON files).  
    * **settings.json** contains the general settings of the training. All the options are discussed below.  
    * **hyperparamters.json**  contains all the hyperparameters and which improvements to conside. All the hyperparameters are dicussed below.  
    * **directory_for_saving_the_checkpoints** has to be the path to a directory in which the checkpoints will be saved; the hyperaparameters and the settings file will also be copied in this directory.  


1. #### Settings:  
	* 'num_runs': Integer, that specifies the number of agents that are trained; DEFAULT: 1
    * 'run_in_parallel': True or False, If the training is parallelized for the number of agents
    * 'seed': seed to reproduce results, here it is left with a value to reproduce our results; DEFAULT: 112358
    * 'use_episodes_as_time': True or False, specify if the numbers of episode is a termination criterion
    * 'num_episodes': None or Integer, Number of episodes that the agent will be trained for before termination (None if unless use_episodes_as_time = True)
    * 'use_iters_as_time': True or False, use number of total iterations (time steps) to terminate training
    * 'num_iterations': Integer, number of iterations that the agent will be trained for before termination (None if unless use_iters_as_time = True)
    * 'save_freq': Integer, every this number of iters the model's performance will be saved
    * 'test_freq': Integer, every this number of iters the model will test episodes
    * 'num_episodes_to_test': Integer, number of episodes that are tested 
    * 'display_frames': True or False, If True the frames of the agent's training will be rendered and displayed
    * 'display_and_save_frames': True or False, If True the frames of the agent's training will be rendered, displayed and saved on a list
    * 'logging_level': Level of detail in the debugging messages (used logging.Warning or logging.INFO) 
    * 'display_pbar': True or False, If True two progress bars are displayed, one for the iterations and one for the episodes
    * 'save_checkpoints': True or False, If the model will be saved (according to the save_freq) or not  
	  
      
2. #### Hyperparameters:
	* 'env_name': The name of the gym environment that will be used for training (e.g. 'Cartpole-v0')
    * 'q_estimator_type': One of QEstimatorsTypes which defines which model to use for predicting the Q-value. Possible choices 0 = Linear Regression, 1 = Multilayer Perceptrons (MLP), 2 = Convolutional Neural Network (CNN) 3 = Dueling MLP, 4 = Dueling CNN  
    * 'optimizer': Adam or RMSprop; the keras optimizer used for training the Q estimator; DEFAULT: Adam
    * 'state_size': The number of variables which defines the current environment's state, i.e. input gym.make('CartPole-v1').observation_space.shape[0] where inside the () is the name of the environment to get that number automatically
    * 'num_frames_per_step': Number of consecutive frames to be stacked in order to build a state
    * 'action_size':Number of possible actions in a state, i.e.input  gym.make('CartPole-v1').action_space.n, where inside the () is the name of the environment
    * 'use_target_network': If True, a separate network is used to compute the target Q-value during training
    * 'use_double_network': If True Double Q-learning is used
    * 'target_network_update_freq': If a use_target_network = True then this specifies the frequency of the update of the Targe network
    * 'negative_reward_value': Negative reward that the agents gets when he loses
    * 'memory_size': number of experinces stored for the experience replay i.e. the size of the buffer
    * 'replay_batch_size': number of experiences to use for training after each iterations
    * 'train_batch_size': the batch size used for updating the q-network in experience replay (must be less or equal than replay_batch_size and a divisor of replay_batch_size)
    * 'gamma': the value of the discount factor
	* "epsilon_initial": the starting value of the exploration/exploitation rate
    * 'epsilon_min': the minumum value that epsilon can reach
    * 'epsilon_decay_rate': the decay rate of epsilon; if it is 'linear', than it will be linearly decayed  based on the maximum number of iterations
    * 'learning_rate': specify the learning rate
    * 'replay_start_size': After how many random actions are performed before the start of experience replay
    * 'final_exploration_step': After this number of iterations the epsilon will be set to epsilon min; if epsilon_decay is 'linear' then this
    * "clipping_rewards": If True, negative rewards are set to -1 and positive rewards to 1 (0 rewards stay the same)
    * "stop_episode_after_each_life": If True, while training, an episode ends when the agent loses a life (only for games that have the concept of different lives per episode)
    * "skip_k_frames": Use the same action for skip_k_frames consecutive frames; this means that the agent will use every skip_k_frames-th frame

 - The (settings and hyperparameters) .json files can be either changed manually (see the examples provided) or by editig the corresponding dictionaries from the script **write_settings_and_hp_to_json.py**. 
 - Finally, a few sbatch scripts are provided which were used to run the algorithms on Peregrine HPC cluster.