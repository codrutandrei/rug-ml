import logging, json, gym

from dqn_agent import QEstimatorsTypes

# training settings; see DQNAgent.train for details
settings_1 = {
    'num_runs': 10,
    'run_in_parallel': True,
    'SEED': 112358,
    'use_episodes_as_time': False,
    'num_episodes': None,
    'use_iters_as_time': True,
    'num_iterations': 10000,
    'save_freq': 1000,
    'test_freq': 100,
    'num_episodes_to_test': 25,
    'display_frames': False,
    'display_and_save_frames': False,
    'logging_level': logging.WARNING,
    'display_pbar': True,
    'save_checkpoints': True,
}

# HPs for 'CartPole-v0'; see DQNAgent constructor for details
hps_cartpole_v0_1 = {
    'env_name': 'CartPole-v0',
    'q_estimator_type': QEstimatorsTypes.MLP,
    'optimizer': 'Adam',
    'state_size': gym.make('CartPole-v0').observation_space.shape[0],
    'num_frames_per_step': 1,
    'action_size': gym.make('CartPole-v0').action_space.n,
    'use_target_network': False,
    'use_double_network': False,
    'target_network_update_freq': None,
    'negative_reward_value': -10,
    'memory_size': 10000,
    'replay_batch_size': 32,
    'train_batch_size': 32,
    'gamma': 0.95,
    "epsilon_initial": 1.0,
    'epsilon_min': 0.05,
    'epsilon_decay_rate': 'linear',
    'learning_rate': 0.001,
    'replay_start_size': 0,
    'final_exploration_step': None,
    "clipping_rewards": False,
    "stop_episode_after_each_life": False,
    "skip_k_frames": 0
}

# HPs for 'CartPole-v1'; see DQNAgent constructor for details
hps_cartpole_v1_1 = {
    'env_name': 'CartPole-v1',
    'q_estimator_type': QEstimatorsTypes.MLP,
    'optimizer': 'Adam',
    'state_size': gym.make('CartPole-v1').observation_space.shape[0],
    'num_frames_per_step': 1,
    'action_size': gym.make('CartPole-v1').action_space.n,
    'use_target_network': False,
    'use_double_network': False,
    'target_network_update_freq': None,
    'negative_reward_value': -10,
    'memory_size': 10000,
    'replay_batch_size': 32,
    'train_batch_size': 32,
    'gamma': 0.95,
    "epsilon_initial": 1.0,
    'epsilon_min': 0.05,
    'epsilon_decay_rate': 'linear',
    'learning_rate': 0.001,
    'replay_start_size': 0,
    'final_exploration_step': None,
    "clipping_rewards": False,
    "stop_episode_after_each_life": False,
    "skip_k_frames": 0
}

# HPs for 'SpaceInvaders-v0'; see DQNAgent constructor for details

hps_space_invaders_1 = {
    'env_name': 'SpaceInvaders-v0',
    'q_estimator_type': QEstimatorsTypes.CNN,
    'optimizer': 'RMSprop',
    'state_size': 84 * 84 * 4,
    'num_frames_per_step': 4,
    'action_size': gym.make('SpaceInvaders-v0').action_space.n,
    'use_target_network': True,
    'use_double_network': False,
    'target_network_update_freq': 10000,
    'negative_reward_value': -10,
    'memory_size': 50000,
    'replay_batch_size': 64,
    'train_batch_size': 64,
    'gamma': 0.99,
    "epsilon_initial": 1.0,
    'epsilon_min': 0.1,
    'epsilon_decay_rate': 'linear',
    'learning_rate': 0.001,
    'replay_start_size': 10000,
    'final_exploration_step': 500000,
    "clipping_rewards": False,
    "stop_episode_after_each_life": False,
    "skip_k_frames": 0
}
if __name__ == "__main__":
    with open('./settings/codrut/settings_1.json', 'w') as fp:
        json.dump(settings_1, fp, indent=4)

    with open('./settings/codrut/hps_cartpole_v0_1.json', 'w') as fp:
        json.dump(hps_cartpole_v0_1, fp, indent=4)

    with open('./settings/codrut/hps_cartpole_v1_1.json', 'w') as fp:
        json.dump(hps_cartpole_v1_1, fp, indent=4)

    with open('./settings/codrut/hps_space_invaders_1.json', 'w') as fp:
        json.dump(hps_space_invaders_1, fp, indent=4)
