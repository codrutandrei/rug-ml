#!/bin/bash
#SBATCH --time=04:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --job-name=cp_s1_h_all_cpu
#SBATCH --mem=32000

module load TensorFlow/1.10.1-fosscuda-2018a-Python-3.6.4
python /home/s3806855/rug-ml/dqn/training_job.py /home/s3806855/rug-ml/dqn/settings/codrut/settings_1.json /home/s3806855/rug-ml/dqn/settings/codrut/hps_cartpole_v0_1.json /home/s3806855/rug-ml/dqn/results/
python /home/s3806855/rug-ml/dqn/training_job.py /home/s3806855/rug-ml/dqn/settings/codrut/settings_1.json /home/s3806855/rug-ml/dqn/settings/codrut/hps_cartpole_v0_2.json /home/s3806855/rug-ml/dqn/results/
python /home/s3806855/rug-ml/dqn/training_job.py /home/s3806855/rug-ml/dqn/settings/codrut/settings_1.json /home/s3806855/rug-ml/dqn/settings/codrut/hps_cartpole_v0_3.json /home/s3806855/rug-ml/dqn/results/
python /home/s3806855/rug-ml/dqn/training_job.py /home/s3806855/rug-ml/dqn/settings/codrut/settings_1.json /home/s3806855/rug-ml/dqn/settings/codrut/hps_cartpole_v0_4.json /home/s3806855/rug-ml/dqn/results/
python /home/s3806855/rug-ml/dqn/training_job.py /home/s3806855/rug-ml/dqn/settings/codrut/settings_1.json /home/s3806855/rug-ml/dqn/settings/codrut/hps_cartpole_v0_5.json /home/s3806855/rug-ml/dqn/results/
