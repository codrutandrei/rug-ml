#!/bin/bash
#SBATCH --time=15:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --job-name=b_s2_h5_gpu
#SBATCH --partition=gpu
#SBATCH --gres=gpu:1
#SBATCH --mem=32000

module load TensorFlow/1.10.1-fosscuda-2018a-Python-3.6.4

python /home/s3806855/rug-ml/dqn/training_job.py /home/s3806855/rug-ml/dqn/settings/codrut/settings_2.json /home/s3806855/rug-ml/dqn/settings/codrut/hps_breakout.json /home/s3806855/rug-ml/dqn/results/

