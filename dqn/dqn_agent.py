import numpy as np
import random as rn
import tensorflow as tf
import os
from keras.models import Sequential, load_model, Model
from keras.layers import Dense, Conv2D, Flatten, Reshape, Add, Lambda, Input
from keras import backend as K
from keras.optimizers import Adam, RMSprop
from tqdm import tqdm as tqdm
import cv2
import gym
import logging
import pickle
import os
import time
import utils


class QEstimatorsTypes:
    LINEAR_REGRESSION = 0
    MLP = 1
    CNN = 2
    DUELING_MLP = 3
    DUELING_CNN = 4


atari_environments = ['SpaceInvaders-v0', 'Breakout-v0']


class DQNAgent:

    def __init__(self, state_size, action_size, num_frames_per_step, q_estimator_type, skip_k_frames=0,
                 optimizer='Adam', use_target_network=False, use_double_network=False, target_network_update_freq=1000,
                 negative_reward_value=-1, clipping_rewards=False, stop_episode_after_each_life=False, memory_size=2000,
                 replay_start_size=0, final_exploration_step=None, replay_batch_size=32, train_batch_size=1, gamma=0.95,
                 epsilon=1.0, epsilon_min=0.01, epsilon_decay_rate=0.995, learning_rate=0.001, seed=112358,
                 save_dir=None):
        """
        :param state_size: the number of variables which defines the current environment's state
        :param action_size: the number of possible actions in a state
        :param num_frames_per_step: how many consecutive frames to stack in order to build a state
        :param q_estimator_type: one of QEstimatorsTypes which defines which model to use for predicting the Q-value
        :param skip_k_frames: use the same action for skip_k_frames consecutive frames; this means that the agent will
                            use every skip_k_frames-th frame
        :param optimizer: Adam or RMSprop; the keras optimizer used for training the Q estimator
        :param use_target_network: if True, a separated network for predicting the target Q-value is used while training
        :param use_double_network: if True, the double Q-learning method is used
        :param target_network_update_freq: the frequency (as a number of training iterations) with which the target
                                           network is updated
        :param negative_reward_value: what reward to give the agent when he loses the game
        :param clipping_rewards: if True, the negative rewards become -1 and the positive ones become 1 (0 stays 0)
        :param stop_episode_after_each_life: if True, while training, an episode ends when the agent loses a life
                                            # this can be used only for games where there are multiple lives
        :param memory_size: the number of experiences to remember for performing experience replay
        :param replay_start_size: how many random actions to perform before starting the experience replay
        :patam final_exploration_step: after this step, the exploration rate will be set to epsilon_min; furthermore,
                                    if the decay rate is 'linear', then epsilon will be linearly annealed up to this nr.
        :param replay_batch_size: how many memories to use for training after each iteration
        :param train_batch_size: the batch size used for updating the q-network in experience replay
                                (must be less or equal than replay_batch_size and a divisor of replay_batch_size)
        :param gamma: the discount rate
        :param epsilon: the initial exploration vs. exploiting rate
        :param epsilon_min: the value at which the epsilon decaying will stop
        :param epsilon_decay_rate: the decay rate of epsilon; if it is 'linear', than it will be linearly decayed
                                    based on the maximum number of iterations
        :param learning_rate: the learning rate used in Q model (e.g. the initial learning rate of Adam optimizer)
        :param seed: the initial random generator seed
        :param save_dir: directory where to save the agent checkpoints
        """
        self.ts = str(utils.now_as_ts())  # timestamp used as model id
        self.state_size = state_size
        self.action_size = action_size
        self.num_frames_per_step = num_frames_per_step
        self.q_estimator_type = q_estimator_type
        self.skip_k_frames = skip_k_frames
        self.optimizer = optimizer
        assert self.optimizer in ['Adam', 'RMSprop']
        self.use_target_network = use_target_network
        self.use_double_network = use_double_network
        self.target_network_update_freq = target_network_update_freq
        self.seed = seed
        self._set_seed()

        self.negative_reward_value = negative_reward_value
        self.clipping_rewards = clipping_rewards
        self.stop_episode_after_each_life = stop_episode_after_each_life
        self.memory_size = memory_size
        self.replay_batch_size = replay_batch_size
        self.train_batch_size = train_batch_size
        assert self.train_batch_size <= self.replay_batch_size and self.replay_batch_size % self.train_batch_size == 0, \
            'The training batch size has to be smaller or equal than the replay batch size and a divisor of it'

        self.memory = self._init_memory()
        self.gamma = gamma
        self.epsilon = epsilon
        self.epsilon_min = epsilon_min
        self.epsilon_decay_rate = epsilon_decay_rate
        self.learning_rate = learning_rate
        self.replay_start_size = replay_start_size
        self.final_exploration_step = final_exploration_step
        self.q_estimator = self._build_model()

        if self.q_estimator is not None:
            logging.debug('Q-value estimator was successfully built.')

        self.target_q_estimator = None
        if self.use_target_network:
            self.target_q_estimator = self._build_model()
            if self.target_q_estimator is not None:
                logging.debug('Target estimator was successfully built.')

        self.last_q_estimator_filename = None
        self.last_ckpt_file = None
        self.save_dir = save_dir
        if self.save_dir is not None and not os.path.isdir(self.save_dir):
            os.makedirs(self.save_dir)

        self.num_episodes_trained = 0
        self.num_iters_trained = 0
        self.training_scores = []
        self.test_scores = []
        self.frames = []  # buffer to save the frames (if necessary)

    def _set_seed(self):
        # Solution from https://stackoverflow.com/questions/46836857
        import os
        os.environ['PYTHONHASHSEED'] = '0'

        # Set numpy rg seed
        np.random.seed(self.seed)

        # Set core Python rg seed
        rn.seed(self.seed)

        # set tf rg seed
        # For further details, see: https://www.tensorflow.org/api_docs/python/tf/set_random_seed
        tf.set_random_seed(self.seed)

        # # uncomment the following lines makes parallel execution is needed (otherwise the results will be non-reproduc.)
        # # Force TensorFlow to use single thread. Multiple threads are a potential source of non-reproducible results.
        # # For further details check https://stackoverflow.com/questions/42022950/
        # session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
        # from keras import backend as K
        # sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
        # K.set_session(sess)

    def _init_memory(self):
        current_states = np.empty(shape=(self.memory_size, self.state_size), dtype=np.float32)
        actions = np.empty(shape=(self.memory_size), dtype=np.uint8)
        rewards = np.empty(shape=self.memory_size, dtype=np.float32)
        next_states = np.empty(shape=(self.memory_size, self.state_size), dtype=np.float32)
        dones = np.empty(shape=self.memory_size, dtype=np.uint8)
        memory = (current_states, actions, rewards, next_states, dones)
        return memory

    def _build_model(self):
        if self.q_estimator_type == QEstimatorsTypes.LINEAR_REGRESSION:
            model = Sequential()
            model.add(Dense(self.action_size, activation='linear'))
        elif self.q_estimator_type == QEstimatorsTypes.MLP:
            model = Sequential()
            model.add(Dense(24, input_dim=self.state_size, activation='relu'))
            model.add(Dense(24, activation='relu'))
            model.add(Dense(self.action_size, activation='linear'))
        elif self.q_estimator_type == QEstimatorsTypes.CNN:
            # architecture from DeepMind paper: Human-level control through deep reinforcement learning
            model = Sequential()
            model.add(Reshape(input_shape=(84 * 84 * self.num_frames_per_step,),
                              target_shape=(84, 84, self.num_frames_per_step)))
            model.add(Conv2D(filters=32, kernel_size=(8, 8), strides=(4, 4),
                             input_shape=(84, 84, self.num_frames_per_step), activation='relu'))
            model.add(Conv2D(64, kernel_size=(4, 4), strides=(2, 2), activation='relu'))
            model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
            model.add(Flatten())
            model.add(Dense(256, activation='relu'))
            model.add(Dense(self.action_size))
        elif self.q_estimator_type == QEstimatorsTypes.DUELING_MLP:
            input_layer = Input(shape=(self.state_size,))
            dense1 = Dense(24, activation='relu')(input_layer)
            dense2 = Dense(24, activation='relu')(dense1)
            state_value = Dense(1)(dense2)
            action_advantage = Dense(self.action_size)(dense2)
            sum_action_advantage = Lambda(lambda x: x - K.mean(x))(action_advantage)
            model = Add()([sum_action_advantage, state_value])
            model = Model(inputs=input_layer, outputs=model)
        elif self.q_estimator_type == QEstimatorsTypes.DUELING_CNN:
            input_layer = Input(shape=(84 * 84 * self.num_frames_per_step,))
            reshaped_layer = Reshape(input_shape=(84 * 84 * self.num_frames_per_step,),
                                     target_shape=(84, 84, self.num_frames_per_step))(input_layer)
            conv1 = Conv2D(filters=32, kernel_size=(8, 8), strides=(4, 4),
                           activation='relu')(reshaped_layer)
            conv2 = Conv2D(64, kernel_size=(4, 4), strides=(2, 2), activation='relu')(conv1)
            conv3 = (Conv2D(64, kernel_size=(3, 3), activation='relu'))(conv2)
            flatten = Flatten()(conv3)
            dense1 = Dense(512, activation='relu')(flatten)
            dense2 = Dense(512, activation='relu')(flatten)
            state_value = Dense(1)(dense1)
            action_advantage = Dense(self.action_size)(dense2)
            sum_action_advantage = Lambda(lambda x: x - K.mean(x))(action_advantage)
            model = Add()([sum_action_advantage, state_value])
            model = Model(inputs=input_layer, outputs=model)
        else:
            return None

        if self.optimizer == 'Adam':
            model.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))
        elif self.optimizer == 'RMSprop':
            model.compile(loss='mse', optimizer=RMSprop(lr=self.learning_rate, rho=0.95, epsilon=0.01))
        return model

    def remember(self, current_state, action, reward, next_state, done):
        # add the new memory; use modulo for overwrite old memories in case the memory is full
        pos_to_write = (self.num_iters_trained - 1) % self.memory_size
        self.memory[0][pos_to_write, :] = current_state.flatten()
        self.memory[1][pos_to_write] = action
        self.memory[2][pos_to_write] = reward
        self.memory[3][pos_to_write, :] = next_state.flatten()
        self.memory[4][pos_to_write] = done

    def get_next_action(self, state):
        # epsilon greedy strategy
        if np.random.rand() <= self.epsilon:  # exploration
            return self._get_random_action()
        else:  # exploitation
            return self.get_greedy_action(state)

    def _get_random_action(self):
        return np.random.randint(0, self.action_size)

    def get_greedy_action(self, state):
        # add a new dimension (as a batch with size 1)
        state = state.flatten()
        act_values = self.q_estimator.predict(x=state.reshape((1,) + state.shape))[0]
        return np.argmax(act_values)

    def replay(self):
        current_num_memories = self.num_iters_trained if self.num_iters_trained < self.memory_size else self.memory_size
        idx_batch = np.sort(np.random.choice(np.arange(current_num_memories), self.replay_batch_size, replace=False))

        # the next line simulates a deque (giving that the memory is implemented as circular list)
        if self.num_iters_trained > self.memory_size:
            idx_batch = (idx_batch + (self.num_iters_trained - self.memory_size)) % self.memory_size

        replay_current_states = self.memory[0][idx_batch]
        replay_actions = self.memory[1][idx_batch]
        replay_rewards = self.memory[2][idx_batch]
        replay_next_states = self.memory[3][idx_batch]
        replay_dones = self.memory[4][idx_batch]

        # train using all replay_batch_size memories using batches of size train_batch_size
        for i in range(int(self.replay_batch_size / self.train_batch_size)):
            current_states = replay_current_states[i * self.train_batch_size:(i + 1) * self.train_batch_size]
            actions = replay_actions[i * self.train_batch_size:(i + 1) * self.train_batch_size]
            rewards = replay_rewards[i * self.train_batch_size:(i + 1) * self.train_batch_size]
            next_states = replay_next_states[i * self.train_batch_size:(i + 1) * self.train_batch_size]
            dones = replay_dones[i * self.train_batch_size:(i + 1) * self.train_batch_size]

            # compute the target Q-value for the current batch
            if not self.use_target_network:
                next_states_q_star = np.max(self.q_estimator.predict(next_states), axis=1)
            else:
                next_states_q_values = self.target_q_estimator.predict(next_states)
                if not self.use_double_network:
                    next_states_q_star = np.max(next_states_q_values, axis=1)
                else:
                    next_states_q_star = next_states_q_values[np.arange(next_states_q_values.shape[0]),
                                                              np.argmax(self.q_estimator.predict(next_states), axis=1)]
            targets = rewards + (1.0 - dones) * self.gamma * next_states_q_star

            # assign the previous computed targets to the corresponding actions that were taken (the rest stay the same)
            targets_to_fit_on = self.q_estimator.predict(current_states)
            targets_to_fit_on[np.arange(targets_to_fit_on.shape[0]), actions] = targets

            # fit the q model
            self.q_estimator.fit(x=current_states, y=targets_to_fit_on, epochs=1, verbose=0)

    def save_training_score(self, score):
        self.training_scores.append(score)

    def save_test_scores(self, scores):
        self.test_scores.append(scores)

    def _decay_epsilon(self, num_iters=None):
        if self.epsilon > self.epsilon_min:
            if self.epsilon_decay_rate == 'linear':
                if self.final_exploration_step is None:
                    i_stop = num_iters
                else:
                    i_stop = self.final_exploration_step
                self.epsilon = 1 - (self.num_iters_trained - self.replay_start_size) / (i_stop - self.replay_start_size)
            else:
                self.epsilon *= self.epsilon_decay_rate
            if self.epsilon < self.epsilon_min or self.num_iters_trained > self.final_exploration_step:
                self.epsilon = self.epsilon_min

    def _update_target_network(self):
        self.target_q_estimator.set_weights(self.q_estimator.get_weights())
        logging.debug('Target network updated (num_iterations_trained = {})'.format(self.num_iters_trained))

    def save_q_estimator(self, filename=None):
        if filename is None:
            filename = 'agent_{}_q_estimator_state_{}_episodes_trained_{}.h5'.format(self.ts, utils.now_as_str(),
                                                                                     self.num_episodes_trained)
            self.last_q_estimator_filename = filename

        f = os.path.join(self.save_dir, filename)
        self.q_estimator.save(f)
        logging.debug("Agent's Q-estimator successfully saved to '{}'".format(f))
        return f

    def load_q_estimator(self, filepath):
        self.q_estimator = load_model(filepath)
        logging.debug("Agent's Q-estimator successfully loaded from '{}'".format(filepath))

    def save_agent_checkpoint(self, filename=None, include_memory=True, is_last_ckpt=False):
        if filename is None:
            filename = 'agent_{}_state_{}_episodes_trained_{}_iterations_trained_{}{}.pk1'.format(
                self.ts, utils.now_as_str(), self.num_episodes_trained, self.num_iters_trained,
                '_last_ckpt' if is_last_ckpt else '')

        f = os.path.join(self.save_dir, filename)

        # if include_memory=False, clear the memory before dump and them restore it
        if not include_memory:
            temp_memory = self.memory
            self.memory = None

        with open(f, 'wb') as fp:
            pickle.dump(self, fp, protocol=pickle.HIGHEST_PROTOCOL)
            logging.debug("Agent successfully saved to '{}'".format(f))
            self.last_ckpt_file = f

        if not include_memory:
            self.memory = temp_memory

        return f

    @staticmethod
    def restore_agent_from_checkpoint(filepath):
        with open(filepath, 'rb') as fp:
            agent = pickle.load(fp)
            logging.debug("Agent state successfully restored from '{}'".format(filepath))
            return agent

    @property
    def summary(self):
        return \
            f"""
    {self.__class__} instance
    ---> ts = {self.ts}
    ---> seed = {self.seed}
    ---> state_size = {self.state_size}
    ---> action_size = {self.action_size}
    ---> num_frames_per_step = {self.num_frames_per_step if hasattr(self, 'num_frames_per_step') else None}
    ---> skip_k_frames = {self.skip_k_frames if hasattr(self, 'skip_k_frames') else None}
    ---> q_estimator_type = {self.q_estimator_type}
    ---> optimizer = {self.optimizer if hasattr(self, 'optimizer') else None}
    ---> use_target_network = {self.use_target_network if hasattr(self, 'use_target_network') else None}
    ---> use_double_network = {self.use_double_network if hasattr(self, 'use_double_network') else None}
    ---> target_network_update_freq = {self.target_network_update_freq if hasattr(self,
                                                                                  'target_network_update_freq') else None}
    ---> negative_reward_value = {self.negative_reward_value}
    ---> clipping_rewards = {self.clipping_rewards if hasattr(self, 'clipping_rewards') else None}
    ---> stop_episode_after_each_life = {self.stop_episode_after_each_life if hasattr(self,
                                                                                      'stop_episode_after_each_life') else None}
    ---> memory_size = {self.memory_size}
    ---> replay_batch_size = {self.replay_batch_size}
    ---> train_batch_size = {self.train_batch_size}
    ---> gamma = {self.gamma}
    ---> epsilon_decay_rate = {self.epsilon_decay_rate}
    ---> epsilon_min = {self.epsilon_min}
    ---> learning_rate = {self.learning_rate}
    ---> replay_start_size = {self.replay_start_size if hasattr(self, 'replay_start_size') else None} 
    ---> final_exploration_step = {self.final_exploration_step if hasattr(self, 'final_exploration_step') else None} 
    ---> num_episodes_trained = {self.num_episodes_trained if hasattr(self, 'num_episodes_trained') else None}
    ---> num_iterations_trained = {self.num_iters_trained if hasattr(self, 'num_iters_trained') else None}
                """

    @staticmethod
    def preprocess_atari_frame(frame):
        # convert to greyscale
        res = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        # resize
        res = cv2.resize(res, (84, 90))
        # crop
        res = res[1:85, :, np.newaxis].astype(np.float32)

        # normalize to [0,1]
        m = np.min(res)
        M = np.max(res)
        if m != M:
            res = (res - m) / (M - m)
        return res

    def train(self, env_name, num_episodes_to_test, display_frames=False, display_and_save_frames=False,
              display_pbar=False, pbar_pos=0, save_ckpts=False, use_episodes_as_time=False, use_iters_as_time=False,
              num_episodes=None, num_iters=None, save_freq=None, test_freq=None):
        """
        :param env_name: the Gym environment name
        :param num_episodes_to_test: how many episodes to test the agent at each interval given by testing frequency
        :param display_frames: if True, the frames will be rendered and displayed
        :param display_and_save_frames: if True, the frames will be rendered, displayed and saved to a list
        :param display_pbar: if True, two progress bars will be used for training
                            (one for episodes and one for iterations)
        :param pbar_pos: position used for tqdm (useful when using multiple threads, see tqdm parameter position)
        :param save_ckpts: if True, the model will be saved based on the specified frequency
        :param use_episodes_as_time: if True, the training will stop after the specified number of episodes
        :param use_iters_as_time: if True, the training will stop after the specified number of iterations
        :param num_episodes: how many episodes to train the agent (if use_episodes_as_time is True)
        :param num_iters: how many iterations to train the agent (if num_episodes_test_freq is True)
        :param save_freq: after how many episodes (if use_episodes_as_time is True)
                        or iterations (if use_iters_as_time is True) to save a checkpoint of the model
        :param test_freq: after how many episodes (if use_episodes_as_time is True)
                        or iterations (if use_iters_as_time is True) to test the agent
        """

        # initialize environment
        env = gym.make(env_name)
        logging.info(f'{env_name} environment started.')

        # check if the training limit is set using the number of episodes or the number of iterations
        assert (use_iters_as_time != use_episodes_as_time)  # only one has to be True

        if use_episodes_as_time:
            assert (num_episodes is not None)
        else:
            assert (num_iters is not None)

        # the maximum number of iterations before the game ends
        max_num_iters_per_episode = env.spec.max_episode_steps

        # set environment seed
        env.seed(self.seed)

        # test the model before starting training
        test_scores, _ = self.evaluate(env_name, num_episodes=num_episodes_to_test)
        self.save_test_scores(test_scores)

        # start training
        stop_training = False

        main_pbar = None
        episode_pbar = None

        reset_env = True
        action = 0

        try:
            if display_pbar:
                main_pbar = tqdm(total=num_episodes if use_episodes_as_time else num_iters, position=pbar_pos)

            while not stop_training:
                # get first state
                if reset_env:
                    current_state = env.reset()
                    reset_env = False
                else:
                    current_state, _, _, _ = env.step(action)

                # preprocess the state for atari games
                if env_name in atari_environments:
                    current_state = self.preprocess_atari_frame(current_state)

                # concatenate num_frames_per_step consecutive frames
                buff = [current_state for _ in range(self.num_frames_per_step)]
                current_state = np.concatenate(buff, axis=-1)

                # variable for summing all the rewards for the current episode
                total_reward_current_episode = 0

                # build a progress bar for the iterations of the current episode
                # (the number of iterations is not known but the maximum is given by the limit of the environment)
                if display_pbar: episode_pbar = tqdm(total=max_num_iters_per_episode, position=pbar_pos + 1)

                done = False  # will be true when an episode ends (i.e. the agent loses the game)
                current_iter = 0  # counter for the current episode's iterations
                while not done:
                    states_buffer = []
                    reward = 0

                    # use the agent to predict the next action based on the current state
                    action = self.get_next_action(state=current_state)

                    # save the current number of lives (before performing the next num_frames_per_step steps)
                    try:
                        last_number_of_lives = info['ale.lives']
                    except:
                        last_number_of_lives = 1

                    # take next num_frames_per_step frames using the previous action and then concatenate them
                    # in order to get the next state; and check if the game ends
                    for i in range(self.num_frames_per_step * (self.skip_k_frames + 1)):
                        # render the current frame (if specified)
                        if display_and_save_frames:
                            current_frame = env.render(mode='rgb_array')
                            self.frames.append(current_frame)
                        elif display_frames:
                            env.render(mode='human')

                        # performed the predicted action in the environment, add the reward and check if the gamed ended
                        singular_next_state, singular_reward, singular_done, info = env.step(action)

                        # clip the reward
                        if self.clipping_rewards:
                            if singular_reward < 0:
                                singular_reward = -1.0
                            elif singular_reward > 0:
                                singular_reward = 1.0

                        reward += singular_reward
                        done |= singular_done

                        # preprocess the next state for space invaders and add it to the buffer
                        if i % (self.skip_k_frames + 1) == 0:
                            if env_name in atari_environments:
                                singular_next_state = DQNAgent.preprocess_atari_frame(singular_next_state)
                            states_buffer.append(singular_next_state)

                    # check again the number of lives (after performing the num_frames_per_step steps)
                    try:
                        current_number_of_lives = info['ale.lives']
                    except:
                        current_number_of_lives = 1

                    # if the agent lost a life, stop the episode; if it was the last life, reset the environment
                    if done:
                        reset_env = True
                    elif self.stop_episode_after_each_life and (current_number_of_lives < last_number_of_lives):
                        done = True

                    # give a negative reward if the episode ended
                    reward = reward if not done else self.negative_reward_value

                    # increment the number of iterations played
                    self.num_iters_trained += 1

                    # concatenate the frames
                    next_state = np.concatenate(states_buffer, axis=-1)

                    # add the reward of the previous concatenated frames to the total reward of the current episode
                    total_reward_current_episode += reward

                    # save the current experience in the agent's memory (for experience replay)
                    self.remember(current_state, action, reward, next_state, done)

                    # check if there are enough memories and if the threshold before starting replayed was achieved
                    if self.num_iters_trained >= self.replay_batch_size and self.num_iters_trained >= self.replay_start_size:
                        # experience replay
                        self.replay()

                        # epsilon-decay
                        self._decay_epsilon(num_iters)

                        # check if the target network is used and if has to be updated
                        if self.use_target_network and self.num_iters_trained % self.target_network_update_freq == 0:
                            self._update_target_network()

                    # update the progress bar of the current episode
                    if display_pbar: episode_pbar.update(1)
                    current_iter += 1

                    # if iterations are used as time measure, then check if the model has to be saved/tested
                    if use_iters_as_time:
                        if display_pbar: main_pbar.update(1)

                        stop_training = (self.num_iters_trained == num_iters)

                        if self.num_iters_trained % test_freq == 0 or stop_training:
                            # evaluate the agent by playing multiple episodes based on greedy actions
                            test_scores, _ = self.evaluate(env_name, num_episodes=num_episodes_to_test)

                            self.save_test_scores(test_scores)

                            logging.info(f'Greedy evaluation score on {len(test_scores)} episodes: '
                                         f'\u03BC = {np.mean(test_scores):.1f}, \u03C3 = {np.std(test_scores):.3f}')

                        if save_ckpts and (self.num_iters_trained % save_freq == 0 or stop_training):
                            self.save_agent_checkpoint(include_memory=False, is_last_ckpt=stop_training)

                        if stop_training:
                            if display_pbar: episode_pbar.close(), main_pbar.close()
                            break

                    # go to next state
                    current_state = next_state

                # close episode bar
                if display_pbar: episode_pbar.close()

                # save the current score after each episode
                self.save_training_score(total_reward_current_episode)

                # increment the number of episodes played
                self.num_episodes_trained += 1

                logging.info(f"Episode {self.num_episodes_trained}/{num_episodes} finished after "
                             f"{current_iter}/{max_num_iters_per_episode} timesteps; "
                             f"score = {total_reward_current_episode}; \u03B5 = {self.epsilon:.2}; "
                             f"num_iterations_trained = {self.num_iters_trained}/{num_iters}")

                if use_episodes_as_time:
                    if display_pbar: main_pbar.update(1)

                    stop_training = (self.num_episodes_trained == num_episodes)

                    if self.num_episodes_trained % test_freq == 0 or stop_training:
                        # evaluate the agent by playing multiple episodes based on greedy actions
                        test_scores, _ = self.evaluate(env_name, num_episodes=num_episodes_to_test)
                        self.save_test_scores(test_scores)
                        logging.info(f'Greedy evaluation score on {len(test_scores)} episodes: '
                                     f'\u03BC = {np.mean(test_scores):.1f}, \u03C3 = {np.std(test_scores):.3f}')
                    if save_ckpts and (self.num_episodes_trained % save_freq == 0 or stop_training):
                        self.save_agent_checkpoint(include_memory=False, is_last_ckpt=stop_training)

                    if stop_training:
                        if display_pbar: episode_pbar.close(), main_pbar.close()
                        break

        except KeyboardInterrupt:
            logging.info('Training interrupted')
            if save_ckpts:
                self.save_agent_checkpoint(include_memory=False, is_last_ckpt=True)

        # stop the environment
        env.close()
        if display_pbar: main_pbar.close()
        logging.info(f'Environment stopped (num_episodes_trained = {self.num_episodes_trained}, '
                     f'num_iterations_trained = {self.num_iters_trained})')

    def evaluate(self, env_name, num_episodes, seed=123, display_frames=False, display_delay=0.5,
                 display_and_save_frames=False, verbose=False):

        # buffer to save the frames (if specified)
        frames = None
        if display_and_save_frames:
            frames = []

        # build the environment and set the seed
        test_env = gym.make(env_name)
        test_env.seed(seed)

        # list for saving the scores after each episode
        test_scores = []

        for i in range(num_episodes):
            # get first state
            current_state = test_env.reset()

            # take a random step (workaround for an issue with the gym atari env - it is stuck if I start with 0)
            if env_name in atari_environments:
                test_env.step(np.random.randint(low=1, high=self.action_size))

            # preprocess the state for atari games
            if env_name in atari_environments:
                current_state = DQNAgent.preprocess_atari_frame(current_state)
            buff = [current_state for _ in range(self.num_frames_per_step)]
            current_state = np.concatenate(buff, axis=-1)

            done = False  # will be true when the episode ends (i.e. the agent loses the game)

            # variable for summing all the rewards for the current episode
            total_reward_current_episode = 0

            current_iter = 0  # counter for the current episode's iterations
            while not done:
                current_iter += 1
                states_buffer = []
                reward, done = 0, False

                # use the agent to predict the next action based on the current state (only exploitation !!!)
                action = self.get_greedy_action(state=current_state.flatten())
                for _ in range(self.num_frames_per_step):
                    # render the current frame (if specified)
                    if display_and_save_frames:
                        current_frame = test_env.render(mode='rgb_array')
                        frames.append(current_frame)
                    elif display_frames:
                        test_env.render(mode='human')

                    # add a delay for after the current frame was rendered
                    if display_and_save_frames or display_frames:
                        time.sleep(display_delay)

                    # performed the predicted action in the environment, add the reward and check if the gamed ended
                    singular_next_state, singular_reward, singular_done, _ = test_env.step(action)
                    reward += singular_reward
                    done |= singular_done

                    # preprocess the next state for atari games and add it to the buffer
                    if env_name in atari_environments:
                        singular_next_state = DQNAgent.preprocess_atari_frame(singular_next_state)
                    states_buffer.append(singular_next_state)

                # concatenate the frames
                next_state = np.concatenate(states_buffer, axis=-1)

                # add the reward of the previous concatenated frames to the total reward of the current episode
                total_reward_current_episode += reward

                # go to next state
                current_state = next_state

            # save the current score
            test_scores.append(total_reward_current_episode)
            if verbose:
                logging.debug(f"Episode {i + 1}/{num_episodes} finished after {current_iter} timesteps, "
                              f"score = {total_reward_current_episode}")

        # stop the testing environment
        test_env.close()
        if verbose: logging.debug('Testing environment stopped')

        return test_scores, frames  # for external usages
