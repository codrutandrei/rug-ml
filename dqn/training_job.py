import logging
import json
import sys
import os
from shutil import copyfile
import numpy as np
import tensorflow  as tf
import keras
from tqdm import tqdm as tqdm
from multiprocessing import Process

# add the path to project directory
project_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_dir)

import utils
from dqn_agent import DQNAgent, QEstimatorsTypes

if __name__ == "__main__":
    assert len(sys.argv) == 4, 'Give the settings, the hyper-parameters (as json file paths) and the directory where' \
                               ' to save the results as command arguments .'

    with open(sys.argv[1], 'r') as fp:
        settings = json.load(fp)

    with open(sys.argv[2], 'r') as fp:
        hps = json.load(fp)

    experiment_root_dir = sys.argv[3]

    print('\nSettings:')
    for k in settings.keys():
        print(f'\t{k} = {settings[k]}')
    print()

    print('\nHyperparameteres:')
    for k in hps.keys():
        print(f'\t{k} = {hps[k]}')

    # get the settings; see DQNAgent.train for details
    num_runs = settings['num_runs']
    run_in_parallel = settings['run_in_parallel']
    SEED = settings['SEED']
    use_episodes_as_time = settings['use_episodes_as_time']
    num_episodes = settings['num_episodes']
    use_iters_as_time = settings['use_iters_as_time']
    num_iterations = settings['num_iterations']
    save_freq = settings['save_freq']
    test_freq = settings['test_freq']
    num_episodes_to_test = settings['num_episodes_to_test']
    display_frames = settings['display_frames']
    display_and_save_frames = settings['display_and_save_frames']
    logging_level = settings['logging_level']
    display_pbar = settings['display_pbar']
    save_checkpoints = settings['save_checkpoints']

    # get the hyperparameters; see DQNAgent constructor for details
    env_name = hps['env_name']
    q_estimator_type = hps['q_estimator_type']
    skip_k_frames = hps['skip_k_frames'] if 'skip_k_frames' in hps.keys() else 0
    state_size = hps['state_size']
    num_frames_per_step = hps['num_frames_per_step']
    action_size = hps['action_size']
    use_target_network = hps['use_target_network']
    use_double_network = hps['use_double_network']
    target_network_update_freq = hps['target_network_update_freq']
    negative_reward_value = hps['negative_reward_value']
    clipping_rewards = hps['clipping_rewards'] if 'clipping_rewards' in hps.keys() else False
    stop_episode_after_each_life = hps[
        'stop_episode_after_each_life'] if 'stop_episode_after_each_life' in hps.keys() else False
    memory_size = hps['memory_size']
    replay_batch_size = hps['replay_batch_size']
    train_batch_size = hps['train_batch_size']
    gamma = hps['gamma']
    epsilon_initial = hps['epsilon_initial'] if 'epsilon_initial' in hps.keys() else 1.0
    epsilon_min = hps['epsilon_min']
    epsilon_decay_rate = hps['epsilon_decay_rate']
    learning_rate = hps['learning_rate']
    replay_start_size = hps['replay_start_size']
    final_exploration_step = hps['final_exploration_step'] if 'final_exploration_step' in hps.keys() else None
    optimizer = hps['optimizer'] if 'optimizer' in hps.keys() else 'Adam'

    # logging setup
    utils.logging_setup_notebook()
    try:
        # check how many resources are allocated
        SLURM_JOB_NUM_NODES = os.environ['SLURM_JOB_NUM_NODES']
        logging.warning(f'SLURM_JOB_NUM_NODES = {SLURM_JOB_NUM_NODES}')

        SLURM_JOB_CPUS_PER_NODE = int(os.environ['SLURM_JOB_CPUS_PER_NODE'])  # Amount of CPUs requested.
        logging.warning(f'SLURM_JOB_CPUS_PER_NODE = {SLURM_JOB_CPUS_PER_NODE}')

        SLURMD_NODENAME = os.environ['SLURMD_NODENAME']
        logging.warning(f'SLURMD_NODENAME = {SLURMD_NODENAME}')

        SLURM_JOB_NODELIST = os.environ['SLURM_JOB_NODELIST']
        logging.warning(f'SLURM_JOB_NODELIST = {SLURM_JOB_NODELIST}')
    except:
        pass

    experiment_dir = None
    if save_checkpoints:
        experiment_dir = f'{experiment_root_dir}/env={env_name}' \
            f'_num_runs={num_runs}' \
            f'_num_episodes={num_episodes if use_episodes_as_time else None}' \
            f'_num_iters={num_iterations if use_iters_as_time else None}' \
            f'_ts={utils.now_as_str_f()}'

        print(f'\nExperiment directory: {experiment_dir}\n')
        if not os.path.isdir(experiment_dir):
            os.makedirs(experiment_dir)
            # save the settings and the hyperparameters in the experiment dir (if save_ckpts if true)
            copyfile(sys.argv[1], os.path.join(experiment_dir, os.path.basename(sys.argv[1])))
            copyfile(sys.argv[2], os.path.join(experiment_dir, os.path.basename(sys.argv[2])))
        else:
            logging.WARNING(f'Experiment directory {experiment_dir} already exists.')

    # set to INFO to suppress agent's debug logs or to ERROR to suppress all the logs
    logging.getLogger().setLevel(logging_level)

    # build the agents
    agents = []
    for seed in SEED + np.arange(num_runs):
        seed = int(seed)
        # build an agent with corresponding HPs and the current seed
        agent = DQNAgent(state_size=state_size, action_size=action_size, num_frames_per_step=num_frames_per_step,
                         q_estimator_type=q_estimator_type, skip_k_frames=skip_k_frames, optimizer=optimizer,
                         use_target_network=use_target_network, use_double_network=use_double_network,
                         target_network_update_freq=target_network_update_freq,
                         negative_reward_value=negative_reward_value, clipping_rewards=clipping_rewards,
                         stop_episode_after_each_life=stop_episode_after_each_life, memory_size=memory_size,
                         train_batch_size=train_batch_size, replay_batch_size=replay_batch_size, gamma=gamma,
                         epsilon=epsilon_initial, epsilon_min=epsilon_min, epsilon_decay_rate=epsilon_decay_rate,
                         learning_rate=learning_rate, replay_start_size=replay_start_size,
                         final_exploration_step=final_exploration_step, seed=seed, save_dir=experiment_dir)

        # add it to the list
        agents.append(agent)

    if not run_in_parallel:
        experiment_pbar = tqdm(total=num_runs, position=0)
        for i, agent in enumerate(agents):
            # start training
            try:
                agent.train(env_name=env_name, num_episodes_to_test=num_episodes_to_test, display_frames=display_frames,
                            display_and_save_frames=display_and_save_frames, display_pbar=display_pbar,
                            pbar_pos=2 * i + 1,
                            save_ckpts=save_checkpoints,
                            use_episodes_as_time=use_episodes_as_time, num_episodes=num_episodes,
                            use_iters_as_time=use_iters_as_time, num_iters=num_iterations,
                            save_freq=save_freq, test_freq=test_freq)
            except KeyboardInterrupt:
                for _ in range(num_runs):
                    print('\n')
                print('Experiment interrupted.')
                break
            experiment_pbar.update(1)
        experiment_pbar.close()
    else:
        processes = []
        for i, agent in enumerate(agents):
            p = Process(target=agent.train, kwargs={'env_name': env_name, 'num_episodes_to_test': num_episodes_to_test,
                                                    'display_frames': display_frames,
                                                    'display_and_save_frames': display_and_save_frames,
                                                    'display_pbar': display_pbar, 'pbar_pos': 2 * i,
                                                    'save_ckpts': save_checkpoints,
                                                    'use_episodes_as_time': use_episodes_as_time,
                                                    'num_episodes': num_episodes,
                                                    'use_iters_as_time': use_iters_as_time, 'num_iters': num_iterations,
                                                    'save_freq': save_freq, 'test_freq': test_freq})
            processes.append(p)

        for p in processes:
            p.start()

        try:
            for p in processes:
                p.join()
        except KeyboardInterrupt:
            pass

    print('Experiment ended.')
