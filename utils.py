from datetime import datetime
import time
import matplotlib.pyplot as plt
from matplotlib import animation
import os
import logging
import sys

plt.rcParams['animation.ffmpeg_path'] = '/home/dcodrut/anaconda3/envs/rugenv/bin/ffmpeg'


def now_as_ts():
    return time.time()


def now_as_str():
    return "{:%Y_%m_%d---%H_%M}".format(datetime.now())


def now_as_str_f():
    return "{:%Y_%m_%d---%H_%M_%f}".format(datetime.now())


def ts_to_str(ts):
    return "{:%Y_%m_%d---%H_%M}".format(datetime.fromtimestamp(ts))


def ts_to_str_f(ts):
    return "{:%Y_%m_%d---%H_%M_%f}".format(datetime.fromtimestamp(ts))


def build_animation(frames, display=True, save_to_file=False, dir_to_save='.', filename=None):
    """
    Displays the frames as an animation using matplotlib and save it to file.

    :param frames: list of frames
    :param display: if True, the animation is displayed
    :param save_to_file: if True, the animation is save to an mp4 file.
    :param dir_to_save: the target directory;
                        default: current directory
    :param filename: the target filename
                     default: current datetime
    """
    plt.figure(figsize=(frames[0].shape[1] / 72.0, frames[0].shape[0] / 72.0), dpi=72)
    patch = plt.imshow(frames[0])
    plt.axis('off')

    def animate(i):
        patch.set_data(frames[i])

    anim = animation.FuncAnimation(plt.gcf(), animate, frames=len(frames), interval=50, repeat=False)

    if save_to_file:
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=20, metadata=dict(artist='gym'), bitrate=1800)
        if filename is not None:
            fn = filename
        else:
            fn = '{}.mp4'.format(now_as_str_f())
        if not os.path.isdir(dir_to_save):
            os.makedirs(dir_to_save)
        fp = os.path.join(dir_to_save, fn)
        anim.save(fp, writer=writer)

    if display:
        plt.show()


def logging_setup():
    f = '[%(asctime)s.%(msecs)03d][%(filename)s][%(funcName)s][%(lineno)s][%(levelname)s][%(message)s]'

    logging.basicConfig(
        level=logging.DEBUG,
        format=f,
        datefmt="%m/%d-%H:%M:%S",
        stream=sys.stdout)


def logging_setup_notebook():
    f = '[%(asctime)s.%(msecs)03d][%(lineno)s][%(levelname)s][%(message)s]'

    logging.basicConfig(
        level=logging.DEBUG,
        format=f,
        datefmt="%m/%d-%H:%M:%S",
        stream=sys.stdout)

