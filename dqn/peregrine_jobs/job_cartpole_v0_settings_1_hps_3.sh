#!/bin/bash
#SBATCH --time=03:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --job-name=s1_h3
#SBATCH --mem=4000

module load TensorFlow/1.12.0-foss-2018a-Python-3.6.4

python /home/s3806855/rug-ml/dqn/training_job.py /home/s3806855/rug-ml/dqn/settings/codrut/settings_1.json /home/s3806855/rug-ml/dqn/settings/codrut/hps_cartpole_v0_3.json /home/s3806855/rug-ml/dqn/results/

