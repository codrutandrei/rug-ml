import gym
import numpy as np
import random
np.random.seed(1337)
random.seed(1337)
from collections import deque
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam


class Qagent():

    def __init__(self):
        #initialize hyperparameters
        self.epsilon = 1.0
        self.epsilon_decay = 0.995
        self.epsilon_min = 0.01
        self.gamma = 0.999
        self.alpha = 0.0001
        self.batch_size = 32
        self.memories = deque(maxlen=100000)
        self.n_episodes = 1000
        self.n_steps = 200
        self.total_steps = []
        #initialize environment
        self.env = gym.make('CartPole-v1')
        self.state_size = self.env.observation_space.shape[0]
        self.action_size = self.env.action_space.n
        #initialize models
        self.q_network = self.build_model()
        self.target_network = self.build_model()
        self.update_target_network()


    def build_model(self):
        model = Sequential()
        model.add(Dense(24, input_dim=self.state_size, activation='relu'))
        model.add(Dense(24, activation='relu'))
        model.add(Dense(self.action_size, activation='linear'))
        model.compile(loss='mse', optimizer=Adam(lr=self.alpha))
        return model


    def update_target_network(self):
        self.target_network.set_weights(self.q_network.get_weights())


    def select_action(self, current_state):
        if random.random() <= self.epsilon:
            return self.env.action_space.sample()
        else:
            qvalue = self.q_network.predict(current_state)
            return np.argmax(qvalue[0])


    def add_memory(self, current_state, action, reward, done, next_state):
        self.memories.append((current_state, action, reward, done, next_state))


    def decay_epsilon(self):
        self.epsilon = max((self.epsilon * self.epsilon_decay), self.epsilon_min)


    def replay_experience(self):
        mini_experience = random.sample(self.memories,
                                   min(len(self.memories), self.batch_size))
        for current_state, action, reward, done, next_state in mini_experience:
            target = reward
            if not done:
                #select action for the next state with Qnetwork
                selected_action = np.argmax(self.q_network.predict(next_state)[0])
                #evaluate the selected action using the target_network
                evaluated_action = self.target_network.predict(next_state)[0][selected_action]
                target = reward + self.gamma * evaluated_action

            target_q = self.q_network.predict(current_state)
            target_q[0][action] = target
            self.q_network.fit(current_state, target_q, epochs=1, verbose=0)


    def run(self):
        solved_limit = 195.0
        times_solved = 0
        for episode in range(self.n_episodes):
            step = 1
            current_state = self.env.reset()
            current_state = np.reshape(current_state, [1, self.state_size])
            done = False

            while step < self.n_steps:
                #self.env.render()
                action = self.select_action(current_state)
                next_state, reward, done, _ = self.env.step(action)
                next_state = np.reshape(next_state, [1, self.state_size])

                #reward = reward if not done else -10
                self.add_memory(current_state, action, reward, done, next_state)
                current_state = next_state
                self.replay_experience()
                self.decay_epsilon()
                if done:
                    break
                step += 1

            print(f"Episode: {episode}, steps: {step}")
            self.total_steps.append(step)
            self.update_target_network()


            if episode>0 and episode%100 == 0:

                average_steps = sum(self.total_steps)/100
                if average_steps > solved_limit:
                    times_solved+=1
                    solved = "True"
                else:
                    solved = "False"
                print(f"The last 100 episodes the average number of steps: \
                {average_steps} and solved = {solved} with epsilon: {self.epsilon}")
                self.total_steps = []



agent = Qagent()
agent.run()



