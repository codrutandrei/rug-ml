import gym
import numpy as np
import random
from collections import deque

class Qagent():


    def __init__(self):

        self.gamma = 0.9
        self.alpha = 0.01
        self.epsilon = 1.0
        self.epsilon_decay = 0.995
        self.epsilon_min = 0.01
        self.n_steps = 201
        self.n_episodes = 1001
        self.batch_size = 32
        self.memories = deque(maxlen=2000)
        self.env = gym.make('CartPole-v1')
        self.state_size = self.env.observation_space.shape[0]
        self.action_size = self.env.action_space.n