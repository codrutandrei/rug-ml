#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 15 19:31:10 2018

@author: doulgeo
"""


"""
These are the steps for the algorithm:
    Initialize the Q-table by all zeros. (dims = States X Actions)
    Start exploring actions: For each state, select any one among all possible
        actions for the current state (S).
    Travel to the next state (S') as a result of that action (a).
    For all possible actions from the state (S') select the one with the highest Q-value.
    Update Q-table values using the equation.
    Set the next state as the current state.
    If goal state is reached, then end and repeat the process.
"""
import numpy as np
import gym
import random
import math

"""
This environment has 5 actions: south,east,west,north,pickup and dropoff.
The aim of the game is to to pick a customer that is in one of 4 posssible fixed
positions on the grid and then to go to the destination which can also be in
one of the four fixed positions. So in other words there is a (5x5) grid . There
are also walls that the taxi cannot go through. The agent gets +20 reward for a 
successful drop-off, -1 penalty for every time step without solution and -10 penalty
for an drop off on a point that isn't the destination.
"""
"""Best run so far: 6 epochs (parameters:
                                         alpha = 0.05
                                         gamma = 0.9
                                         decay_rate = 0.1
                                         epsilon=0.8
                                         method of reducing epsilon=continuous)
"""
env = gym.make('Taxi-v2').env
#Visualize the environmnet to help understanding it
env.reset()
env.render()

"""Q-learning Implementation"""

#Initialize the Q-table
n_actions = env.action_space.n
n_states = env.observation_space.n
Q_table = np.zeros([n_states, n_actions])

#Hyperparameters 
alpha = 0.05 #Learning rate like in supervised learning
gamma = 0.9 #Gamma close to 1 makes the agent less greedy


decay_rate = 0.01

max_episodes = 10000


num_epochs = []
for i in range(max_episodes):
    state = env.reset()
    epochs = 0
    penalties, rewards = 0, 0
    done = False
    frames = []
    epsilon = 0.8
    
    while not done:
        ti = random.uniform(0,1) 
        if ti < epsilon:
            action = env.action_space.sample() #explore enivronment
        else:
            action = np.argmax(Q_table[state]) #Act according to Q-table
            
        nxt_state, reward, done, info = env.step(action)
        exp_fut_aw = np.max(Q_table[nxt_state])
         
        #Update Q-values using Bellman's Equation
        Q_table[state, action] = Q_table[state,action] + alpha*(reward 
               + gamma*exp_fut_aw - Q_table[state,action])
        
        state = nxt_state #Update state
        
        if reward == -10:
            penalties += 1
        
        
        epochs += 1
        """2 ways of reducing epsilon, one is periodically, e.g. every
        100 episodes, the other reducing little by little every episode"""
        #if epochs%100 == 0:  #Every 100 episodes reduce the value of epsilon
        #   epsilon = epsilon*decay_rate
        epsilon = epsilon/(epochs+1)
        #frames.append({'frame' : env.render()})

    if i%500 == 0:
        
        print('Done after {} epochs\n'.format(epochs))
    num_epochs.append(epochs)
    
        
    

print("Training Finished \n")
print('Minimum number of epochs: {}'.format(min(num_epochs)))
        
        
        
        
        
        