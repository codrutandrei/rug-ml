#!/bin/bash
#SBATCH --time=12:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --job-name=si_s2_h2_gpu
#SBATCH --partition=gpu
#SBATCH --gres=gpu:1
#SBATCH --mem=48000

module load TensorFlow/1.12.0-foss-2018a-Python-3.6.4

python /home/s3806855/rug-ml/dqn/training_job.py /home/s3806855/rug-ml/dqn/settings/codrut/settings_2.json /home/s3806855/rug-ml/dqn/settings/codrut/hps_space_invaders_2.json /home/s3806855/rug-ml/dqn/results/

